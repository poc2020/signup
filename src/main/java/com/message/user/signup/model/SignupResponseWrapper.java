package com.message.user.signup.model;

import org.springframework.stereotype.Component;

@Component
public class SignupResponseWrapper {
	private String firstName;
	private String lastName;
	private String phone;

	public SignupResponseWrapper() {
		// TODO Auto-generated constructor stub
	}

	public SignupResponseWrapper(String firstName, String lastName, String phone) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phone;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "SubscriberResponse [firstName=" + firstName + ", lastName=" + lastName + ", phone=" + phone + "]";
	}

}
