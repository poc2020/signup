package com.message.user.signup.util;

import com.message.user.signup.exception.DataValidationException;
import com.message.user.signup.model.Signup;

public class SignupValidationUtil {
	public  boolean validateRequest(Signup signup) {
		try {
			if (signup.getFullName().isEmpty() | signup.getPhone().isEmpty()) {
				return false;
			}
		} catch (DataValidationException e) {
			// TODO: handle exception
			throw new DataValidationException("signup request shouldn't be null");
		}
		return true;
	}
}
