package com.message.user.signup.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.message.user.signup.util.SignupValidationUtil;

@Configuration
public class ApplicationConfiguration {
	@Bean
	public SignupValidationUtil getUtilInstance() {
		return new SignupValidationUtil();
	}
}
