package com.message.user.signup.exception;

public class DataValidationException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DataValidationException(String message) {
		// TODO Auto-generated constructor stub
		super(message);
	}
}
