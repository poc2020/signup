package com.message.user.signup.service.impl;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.message.user.signup.model.Signup;
import com.message.user.signup.service.MessagePublisher;
@Service
public class SignupMessagePublisherImpl implements MessagePublisher{
	@Autowired
	private AmqpTemplate rabbitTemplate;
	
	@Value("${signup.rabbitmq.exchange}")
	private String exchange;
	
	@Value("${signup.rabbitmq.routingkey}")
	private String routingkey;	
	
	@Override
	public void message(Signup singup) {
		// TODO Auto-generated method stub
		rabbitTemplate.convertAndSend(exchange, routingkey, singup);
		System.out.println("Send msg = " + singup);
	}
}
