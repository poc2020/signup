package com.message.user.signup.service.impl;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.message.user.signup.model.Signup;
import com.message.user.signup.model.SignupResponseWrapper;
import com.message.user.signup.service.MessagePublisher;
import com.message.user.signup.util.ApplicationConstant;
import com.message.user.signup.util.FrenchPhoneValidationUtil;

@Component
public class SignupMessageConsumerImpl implements MessagePublisher {

	Logger log = LogManager.getLogger(SignupMessageConsumerImpl.class);
	@Autowired
	SignupResponseWrapper signUp;
	/**
	 * We have created a threapool size of 5 threads to process the data coming from
	 * Rabbit MQ
	 */

	ExecutorService threadPool = null;

	public SignupMessageConsumerImpl() {
		// TODO Auto-generated constructor stub
		threadPool = Executors.newFixedThreadPool(5);

	}

	public SignupResponseWrapper getSignUp() {
		return signUp;
	}

	public void setSignUp(SignupResponseWrapper signUp) {
		this.signUp = signUp;
	}

	@Override
	public void message(Signup singup) {
		// TODO Auto-generated method stub
		processor(singup);
	}

	@RabbitListener(queues = "${signup.rabbitmq.queue}")
	public void getMessage(Signup obj) {

		threadPool.execute(new Runnable() {

			@Override
			public void run() {
				message(obj);
			}
		});
	}

	public void processor(Signup obj) {
		// TODO Auto-generated method stub
		String fullName = obj.getFullName();
		int idx = fullName.lastIndexOf(' ');
		if (idx == -1)
			throw new IllegalArgumentException("Only a single name: " + fullName);
		String firstName = fullName.substring(0, idx);
		String lastName = fullName.substring(idx + 1);

		log.info("firstanme:" + firstName);
		log.info("lastname: " + lastName);
		String phoneNumber = obj.getPhone();
		String removedSpaces = FrenchPhoneValidationUtil.removeSpaceInBetweenString(phoneNumber);

		log.info("after removing spaces in phonenumber " + removedSpaces);
		String code = removedSpaces.substring(0, 2);
		if (!code.endsWith(ApplicationConstant.AREA_CODE)) {
			removedSpaces = ApplicationConstant.AREA_CODE + removedSpaces;
		}
		log.info(removedSpaces);
		signUp.setFirstName(firstName);
		signUp.setLastName(lastName);
		signUp.setPhone(removedSpaces);
		log.info("User " + fullName + " with phone " + removedSpaces + "has just signed up!");

		System.out.println(
				"Members Registered by Barry :" + signUp + "processed by --->" + Thread.currentThread().getName());

	}

}
